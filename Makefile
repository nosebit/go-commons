SHELL := /bin/bash
PATH := node_modules/.bin:$(PATH)

.PHONY: init deps install run

init:
	@echo ">> Initializing go-commons"

	@$(MAKE) deps

deps:
	@echo "-- Installing dependencies"
	dep ensure

install:
	@echo ">> Installing go-commons"
	@go install

run:
	bash scripts/run.sh
